<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PingApiTest extends TestCase
{
    /**
     * A basic test to know if the ApiPing is working properly
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/api/ping');

        $response->assertStatus(200);
    }
}
